Feature: Create Lead for LeafTaps

Background:
Given Open Chrome browser
And Maximize the browser
And Set the timeout
And Enter the URL
Given Open Chrome browser
And Maximize the browser
And Set the timeout
And Enter the URL
And Enter the username as
And Enter the password as
And Click on Login Button
And Click CRMSFA
And Click Create Lead


@smoke @sanity
Scenario Outline: Positive Flow
And Enter Company Name as
And Enter First Name as
And Enter Last Name as
When Click Submit Button
Then Verify Lead is Created
Examples:
|username|password|
|DemoSalesManager|crmsfa1|
|DemoSalesManager1|crmsfa|

Scenario: Negative Flow
And Enter First Name as
And Enter Last Name as
When Click Submit Button
But Verify Lead is Not Created


