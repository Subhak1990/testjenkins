package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class CreateLead extends ProjectMethods{

	public CreateLead() {
		//apply PageFactory
				PageFactory.initElements(driver, this); 
	}
	@FindBy(how = How.ID,using="createLeadForm_firstName") WebElement eleFname;
     public CreateLead fName(String fName) {
		clearAndType(eleFname, fName);
		return this;
	}
 	@FindBy(how = How.ID,using="createLeadForm_lastName") WebElement eleLname;

     public CreateLead lName(String LName) {
    	 clearAndType(eleLname, LName);
    	 return this;
		
	}
 	@FindBy(how = How.ID,using="createLeadForm_companyName") WebElement eleCname;

     public CreateLead cName(String CName) {
	  clearAndType(eleCname, CName);
	  return this;
      }
     
  	@FindBy(how = How.ID,using="submitButton") WebElement eleClick;

      public ViewLead creatLead() {
	  click(eleClick);
	  return new ViewLead();
}
}
