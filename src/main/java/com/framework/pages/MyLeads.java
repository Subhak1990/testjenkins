package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class MyLeads extends  ProjectMethods{
  
	public MyLeads()
	{
		//apply PageFactory
		PageFactory.initElements(driver, this); 
	}
	
	@FindBy(how=How.PARTIAL_LINK_TEXT,using="Create Lead") WebElement elecLead;
	public CreateLead createLead() {
		click(elecLead);
		return new CreateLead();
		
	}
}
