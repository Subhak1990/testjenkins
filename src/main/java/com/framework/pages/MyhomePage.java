package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class MyhomePage extends ProjectMethods {
	
	public MyhomePage()
	{
		//apply PageFactory
		PageFactory.initElements(driver, this); 
	}
	
	@FindBy(how=How.PARTIAL_LINK_TEXT,using="Leads") WebElement eleLeads;
	public MyLeads clickLeadstab() {
		click(eleLeads);
		return new MyLeads();
		
	}

}
