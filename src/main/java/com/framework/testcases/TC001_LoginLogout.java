package com.framework.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.framework.design.ProjectMethods;
import com.framework.pages.LoginPage;

public class TC001_LoginLogout extends ProjectMethods 
{
	@BeforeTest
	public void setData()
	{
		testCaseName="TC001_LoginLogout";
		testDescription="Login";
		testNodes="Leads";
		author="Subha";
		category="Smoke";
		dataSheetName="TC001";
	}
	@Test(dataProvider="fetchData")
	public void login(String username,String password) {
		new LoginPage().enterUsername(username).enterPassword(password).clickLogin().clickLogout();
//		LoginPage lp= new LoginPage();
//		lp.enterUsername("");
//		lp.enterPassword("");
//		lp.clickLogin();
	}


}