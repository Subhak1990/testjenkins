package steps;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.But;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class createLeadSteps {
	
	public ChromeDriver driver;
	
	@Given("Open Chrome browser")
	public void openChromeBrowser() {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		 driver=new ChromeDriver();
	}

	@Given("Maximize the browser")
	public void maximizeTheBrowser() {
		
		driver.manage().window().maximize();
	}

	@Given("Set the timeout")
	public void setTheTimeout() {
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}

	@Given("Enter the URL")
	public void enterTheURL() {
		
		driver.get("http://leaftaps.com/opentaps/");
	}

	@Given("Enter the username as")
	public void enterTheUsernameAs() {
		driver.findElementById("username").sendKeys("DemoSalesManager");
	}

	@Given("Enter the password as")
	public void enterThePasswordAs() {
		driver.findElementById("password").sendKeys("crmsfa");
	}

	@Given("Click on Login Button")
	public void clickOnLoginButton() {
        driver.findElementByClassName("decorativeSubmit").click();
	}

	@Given("Click CRMSFA")
	public void clickCRMSFA() {
    driver.findElementByPartialLinkText("CRM/SFA").click();
	}

	@Given("Click Create Lead")
	public void clickCreateLead() {
		driver.findElementByPartialLinkText("Create Lead").click();
	}

	@Given("Enter Company Name as")
	public void enterCompanyNameAs() {
	    driver.findElementById("createLeadForm_companyName").sendKeys("Infosys");
	}

	@Given("Enter First Name as")
	public void enterFirstNameAs() {
	    driver.findElementById("createLeadForm_firstName").sendKeys("Maya");

	}

	@Given("Enter Last Name as")
	public void enterLastNameAs() {
	    driver.findElementById("createLeadForm_lastName").sendKeys("Vijay");
	}

	@When("Click Submit Button")
	public void clickSubmitButton() {
driver.findElementByName("submitButton").click();
	}

	@But("Verify Lead is Not Created")
	public void verifyLeadIsCreatedFailed() {
		String text = driver.findElementByClassName("errorMessage").getText();
		if(text=="The following required parameter is missing: [crmsfa.createLead.companyName]")
		{
			System.out.println("Error message occuring");
		}
		else
		{
	        System.out.println("Erroe msg not occuring");   
		}
	}
	@Then("Verify Lead is Created")
	public void verifyLeadIsCreated() {
		    System.out.println("Verified");   
		
	}





}
